﻿using NUnit.Framework;
using PankkitiliSovellus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PankkitiliSovellusTests
{
    [TestFixture]
    public class PankkitiliTests
    {
        /* Pankkitilisovellus sisältää:
         *
         * Luokka, jonka nimi on pankkitili
         * Pankkitilissä on kolme toimintoa
         * 
         * -Tallettaa rahaa
         * -Nostaa rahaa
         * --Tarkistetaan, ettei tili mene miinukselle
         * -Siirtää rahaa tililtä toiselle
         * --Tarkistetaan, ettei tili mene miinukselle
         * 
         */

        public Pankkitili tili1 = null;

         [SetUp]
         public void TestienAlustaja()
        {
            this.tili1 = new Pankkitili(100);
        }

        [Test]
        public void LuoPankkitili()
        {
            //Tällä tarkistetaan minkä tyyppinen olio on.
            Assert.IsInstanceOf<Pankkitili>(tili1);
        }

        [Test]
        public void AsetaPankkitililleAlkusaldo()
        {
            Assert.That(100, Is.EqualTo(tili1.Saldo));
        }

        [Test]
        public void TalletaRahaaPankkitilille()
        {
            //Talletetaan tilille rahaa
            tili1.Talleta(500);
            Assert.That(600, Is.EqualTo(tili1.Saldo));
        }

        [Test]
        public void NostaPankkitililtaRahaa()
        {
            tili1.NostaRahaa(75);
            Assert.That(25, Is.EqualTo(tili1.Saldo));
        }

        [Test]
        public void NostonJalkeenPankkitiliEiVoiOllaMiinuksella()
        {
            //Testataan, että antaako ohjelma halutun virhetyypin
            Assert.Throws<ArgumentException>(() => tili1.NostaRahaa(175));
            //Vaikka virhe sattuu, niin rahoja ei menetetä.
            //Pitäisi olla loppusaldon kanssa sama.
            Assert.That(100, Is.EqualTo(tili1.Saldo));
        }
    }
}
