﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PankkitiliSovellus
{
    public class Pankkitili
    {
        public int Saldo { get; set; }

        public Pankkitili(int alkusaldo)
        {
            Saldo = alkusaldo;
        }

        public void Talleta(int summa)
        {
            Saldo += summa;
        }

        public void NostaRahaa(int summa)
        {
            if (Saldo < summa)
            {
                throw new ArgumentException("Tilillä ei ole tarpeeksi rahaa.");
            }
            Saldo -= summa;         
        }
    }
}
